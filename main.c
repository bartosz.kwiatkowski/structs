#include <stdio.h>
#include <stdlib.h>
#include "Test.h"

int main(){

    Square kwadracik;
    
    kwadracik = constructor(10, 10);

    printf("Kwadrat ma wymiary: %d  x %d \n", kwadracik.x, kwadracik.y);

    printf("Pole kwadratu to %.2f \n", field(kwadracik));

    addComment("wiadomosc");
    printComment();

    addComment("To bedzie dodatkowy komentarz!");
    printComment();

    return 0;
}