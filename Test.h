struct Square
{
    int x;
    int y;
    char comment[100];
};

typedef struct Square Square;

Square constructor(int x, int y);

float field(Square squareObj);

void addComment(char string[]);

void printComment();


