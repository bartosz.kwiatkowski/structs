#include "Test.h"
#include <stdio.h>
#include <string.h>

struct Square square;

Square constructor(int x, int y){

    square.x = x;
    square.y = y;
    
    return square;
}

float field(Square squareObj){
    return squareObj.x * squareObj.y;
}


void addComment(char string[]){

    if (square.comment[0] == '#'){
        strcat(square.comment, " ");
        strcat(square.comment, string);
    } else {
        strcpy(square.comment, "# ");
        strcat(square.comment, string);
    }
    
}

void printComment(){
    printf("%s \n", square.comment);
}

